---
stage: Govern
group: Anti-Abuse

To report abuse from a user's profile page:

1. Anywhere in GitLab, select the name of the user.
1. In the upper-right corner of the user's profile select the vertical ellipsis (**{ellipsis_v}**), then **Report abuse to administrator**.
1. Select a reason for reporting the user.
1. Complete an abuse report.
1. Select **Send report**.

## Report abuse from a user's comment

> Reporting abuse from comments in epics [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/389992) in GitLab 15.10.

To report abuse from a user's comment:

1. In the comment, in the upper-right corner, select **More actions** (**{ellipsis_v}**).
1. Select **Report abuse to administrator**.
1. Select a reason for reporting the user.
1. Complete an abuse report.
1. Select **Send report**.

NOTE:
A URL to the reported user's comment is pre-filled in the abuse report's
**Message** field.

## Report abuse from an issue
1. Complete an abuse report.
1. Select **Send report**.

## Report abuse from a user's comment

> Reporting abuse from comments in epics [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/389992) in GitLab 15.10.

To report abuse from a user's comment:

1. In the comment, in the upper-right corner, select **More actions** (**{ellipsis_v}**).
1. Select **Report abuse to administrator**.
1. Select a reason for reporting the user.
1. Complete an abuse report.
1. Select **Send report**.

1. Complete an abuse report.
1. Select **Send report**.
1. In the comment, in the upper-right corner, select **More actions** (**{ellipsis_v}**).
1. Select **Report abuse to administrator**.
1. Select a reason for reporting the user.
1. Complete an abuse report.
1. Select **Send report**.

1. Complete an abuse report.
1. Select **Send report**

## Report abuse from a user's comment

> Reporting abuse from comments in epics [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/389992) in GitLab 15.10.

To report abuse from a user's comment:

1. In the comment, in the upper-right corner, select **More actions** (**{ellipsis_v}**).
1. Select **Report abuse to administrator**.
1. Select a reason for reporting the user.
1. Complete an abuse report.
1. Select **Send report**.


1. On the issue, in the upper-right corner, select **Issue actions** (**{ellipsis_v}**).
1. Select **Report abuse to administrator**.
1. Select a reason for reporting the user.
1. Complete an abuse report.
1. Select **Send report**.

## Report abuse from a merge request

1. On the merge request, in the upper-right corner, select **Merge request actions** (**{ellipsis_v}**).
1. Select **Report abuse to administrator**.
1. Select a reason for reporting this user.
1. Complete an abuse report.
1. Select **Send report**.

## Related topics

- [Abuse reports administration documentation](../administration/review_abuse_reports.md)
