---
stage: AI-powered
group: Duo Chat
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: index, reference
---

# Answer questions with GitLab Duo Chat **(ULTIMATE SAAS EXPERIMENT)**

> Introduced in GitLab 16.0 as an [Experiment](../policy/experiment-beta-support.md#experiment).

You can get AI generated support from GitLab Duo Chat about the following topics:

- How to use GitLab.
- Questions about an issue.
- How to use GitLab.
- Questions about an issue.
- Question about an epic.




- Questions about a code file.
- Follow-up questions to answers from the chat.



Example questions you might ask:

- `Explain the concept of a 'fork' in a concise manner.`
- `Provide step-by-step instructions on how to reset a user's password.`
- `Generate a summary for the issue identified via this link: <link to your issue>`
- `Generate a concise summary of the description of the current issue.`

The examples above all use data from either the issue or the GitLab documentation. However, you can also ask to generate code, CI/CD configurations, or to explain code. For example:

- `Write a Ruby function that prints 'Hello, World!' when called.`
- `Develop a JavaScript program that simulates a two-player Tic
-
- -Tac-Toe game. Provide both game logi
-
- c and user interface, if applicable.`
- `Create a .gitlab-ci.yml configuration file for testing and building a Ruby on Rails application in a GitLab CI/CD pipeline.`
- `Provide a clear explanation of the given Ruby code: def sum(a, b) a + b end. Describe what this code does and how it works.`

In addition to the provided prompts, feel free to ask follow-up questions to delve deeper into the topic or task at hand. This helps you get more detailed and precise responses tailored to your specific needs, whether it's for further clarification, elaboration, or additional assistance.

- A follow-up to the question `Write a Ruby function that prints 'Hello, World!' when called.` could be:
  - `Could you also explain how I can call and execute this Ruby function in a typical Ruby environment, such as the command line?`

This is an experimental feature and we're continuously extending the capabilities and reliability of the chat.


## Give Feedback

Your feedback is important to us as we continually enhance your GitLab Duo Chat experience:

- **Enhance Your Experience**: Leaving feedback helps us customize the Chat for your needs and improve its performance for everyone.
- **Privacy Assurance**: Rest assured, we don't collect your prompts. Your privacy is respected, and your interactions remain private.

To give feedback about a specific response, use the feedback buttons in the response message.
Or, you can add a comment in the [feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/415591).
