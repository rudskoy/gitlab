stage: Data Stores
group: Tenant Scale
info: To determine the technical writer assigned to the Stage/Group associated with this page, see [Technical Writing Assignments](https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments).
---

# Reserved Project and Group Names **(FREE ALL)**

Not all project and group names are allowed because they would conflict with existing routes used by GitLab.

For a list of words that are not allowed to be used as group or project names, consult the [`path_regex.rb` file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/path_regex.rb) under the `TOP_LEVEL_ROUTES`, `PROJECT_WILDCARD_ROUTES`, and `GROUP_ROUTES` lists:

- `TOP_LEVEL_ROUTES`: Names that are reserved for usernames or top-level groups.
- `PROJECT_WILDCARD_ROUTES`: Names that are reserved for child groups or projects.
- `GROUP_ROUTES`: Names that are reserved for all groups or projects.


## Reserved Project Names

You cannot create a project with the following names:

- `\-`
- `badges`
- `blame`
- `blob`
- `builds`
- `commits`
- `create`
- `create_dir`
- `edit`
- `environments/folders`
- `files`
- `find_file`
- `gitlab-lfs/objects`
- `info/lfs/objects`
- `new`
- `preview`
- `raw1`
- `refs2`
- `tree3`
- `update4`
- `wikis`

## Reserved Group Names

The following names are reserved for top-level groups:

- `\-`
- `.well-known`
- `404.html`
- `422.html`
- `500.html`
- `502.html`
- `503.html`
- `admin`
- `api`
- - `deploy.html`
- `explore`
- `favicon.ico`
- `favicon.png`
- `files`
- `groups`
- `health_check`
- `help3`
- `import2`
- `jwt1`
- `login`
- `apple-touch-icon.png`
- `assets`
- `dashboard`
- `deploy.html`
- `explore`
- `favicon.ico`
- `favicon.png`
- `files`
- `groups`
- `health_check`
- `help3`
- `import2`
- `jwt1`
- `login`
- `oauth`
- `profile`
- `projects`
- `public`
- `robots.txt`
- `s`
- `search`
- `sitemap`
